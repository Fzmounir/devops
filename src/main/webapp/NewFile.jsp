<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Bid Table</title>
    <style>
        /* Styles for table */
        table {
            border-collapse: collapse;
            width: 100%;
        }
        th, td {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        tr:nth-child(even) {
            background-color: #f2f2f2;
        }
        /* Styles for form container */
        .form-container {
            max-width: 300px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
            background-color: #f9f9f9;
        }
        .form-container label {
            display: block;
            margin-bottom: 10px;
        }
        .form-container input[type=text], .form-container input[type=number] {
            width: calc(100% - 22px);
            padding: 10px;
            margin-bottom: 20px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }
        .form-container input[type=date] {
            width: 100%;
        }
        .form-container select {
            width: 100%;
            padding: 10px;
            margin-bottom: 20px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }
        .form-container input[type=submit] {
            background-color: #4CAF50;
            color: white;
            padding: 10px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
        .form-container input[type=submit]:hover {
            background-color: #45a049;
        }
    </style>
    <script>
        function searchTable() {
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("searchInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("bidTable");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td");
                for (var j = 0; j < td.length; j++) {
                    if (td[j]) {
                        txtValue = td[j].textContent || td[j].innerText;
                        if (txtValue.toUpperCase().indexOf(filter) > -1) {
                            tr[i].style.display = "";
                            break;
                        } else {
                            tr[i].style.display = "none";
                        }
                    }
                }
            }
        }

        function deleteRow(btn) {
            var row = btn.parentNode.parentNode;
            row.parentNode.removeChild(row);
        }

        function modifyRow(btn) {
            var row = btn.parentNode.parentNode;
            var id = row.cells[0].textContent;
            var bid = row.cells[1].textContent;
            var idProduct = row.cells[2].textContent;
            var idBuyer = row.cells[3].textContent;
            var idSeller = row.cells[4].textContent;
            var message = row.cells[5].textContent;
            var dateCreate = row.cells[6].textContent;
            var dateUpdate = row.cells[7].textContent;
            var active = row.cells[8].textContent;
            var url = "BidServlet?id=" + id + "&bid=" + bid + "&idProduct=" + idProduct + "&idBuyer=" + idBuyer + "&idSeller=" + idSeller + "&message=" + encodeURIComponent(message) + "&dateCreate=" + dateCreate + "&dateUpdate=" + dateUpdate + "&active=" + active;
            window.location.href = url; // Redirect to servlet for modification
        }
    </script>
</head>
<body>
    <h2>Bid Table</h2>
    <div>
        <input type="text" id="searchInput" onkeyup="searchTable()" placeholder="Search for bids..">
    </div>
    <table id="bidTable">
        <tr>
            <th>id</th>
            <th>bid</th>
            <th>idProduct</th>
            <th>idBuyer</th>
            <th>idSeller</th>
            <th>message</th>
            <th>dateCreate</th>
            <th>dateUpdate</th>
            <th>active</th>
            <th>Action</th>
        </tr>
        <!-- Displaying bids from the servlet -->
        <c:forEach items="${bids}" var="bid">
            <tr>
                <td>${bid.id}</td>
                <td>${bid.bid}</td>
                <td>${bid.idProduct}</td>
                <td>${bid.idBuyer}</td>
                <td>${bid.idSeller}</td>
                <td>${bid.message}</td>
                <td>${bid.dateCreate}</td>
                <td>${bid.dateUpdate}</td>
                <td>${bid.active}</td>
                <td>
                    <button onclick="deleteRow(this)" name="action" value="delete">Delete</button>
                    <button onclick="modifyRow(this)" name="action" value="modify">Modify</button>
                    <input type="hidden" name="index" value="${loop.index - 1}">
                </td>
            </tr>
        </c:forEach>
    </table>

    <div class="form-container">
        <h2>Add Bid</h2>
        <form action="BidServlet" method="post">
            <label for="bid">Bid:</label>
            <input type="number" id="bid" name="bid" required>
            <label for="idProduct">Product ID:</label>
            <input type="number" id="idProduct" name="idProduct" required>
            <label for="idBuyer">Buyer ID:</label>
            <input type="number" id="idBuyer" name="idBuyer" required>
            <label for="idSeller">Seller ID:</label>
            <input type="number" id="idSeller" name="idSeller" required>
            <label for="message">Message:</label>
            <input type="text" id="message" name="message">
            <label for="dateCreate">Date Create:</label>
            <input type="date" id="dateCreate" name="dateCreate" required>
            <label for="dateUpdate">Date Update:</label>
            <input type="date" id="dateUpdate" name="dateUpdate" required>
            <label for="active">Active:</label>
            <select id="active" name="active" required>
                <option value="true">True</option>
                <option value="false">False</option>
            </select>
            <input type="submit" value="Submit">
        </form>
    </div>
</body>
</html>

