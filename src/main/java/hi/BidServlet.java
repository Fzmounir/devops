package hi;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

@WebServlet("/BidServlet")
public class BidServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private ArrayList<Bid> bids = new ArrayList<>();

    public BidServlet() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Check if the request is for deleting a bid
        String action = request.getParameter("action");
        if (action != null && action.equals("delete")) {
            // Get the index of the bid to be deleted
            int index = Integer.parseInt(request.getParameter("index"));
            // Remove the bid from the bids list
            bids.remove(index);
            // Forward the request to the JSP page for rendering
            request.setAttribute("bids", bids);
            request.getRequestDispatcher("/NewFile.jsp").forward(request, response);
        } else if (action != null && action.equals("modify")) {
            // Get the index of the bid to be modified
            int index = Integer.parseInt(request.getParameter("index"));
            // Forward the request to a new JSP page for modifying the bid
            request.setAttribute("bidIndex", index);
            request.getRequestDispatcher("/ModifyBid.jsp").forward(request, response);
        } else {
            // If no action specified, add bid information from form to table
            String bidderName = request.getParameter("bidderName");
            double bidAmount = Double.parseDouble(request.getParameter("bidAmount"));
            Bid bid = new Bid(bidderName, bidAmount);
            bids.add(bid);
            request.setAttribute("bids", bids);
            request.getRequestDispatcher("/NewFile.jsp").forward(request, response);
        }
    }
    
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String apiUrl = "http://localhost:8080/api-0.0.1/swagger-ui/#/Bids/getBids";

        // Create URL object
        @SuppressWarnings("deprecation")
		URL url = new URL(apiUrl);

        // Create HttpURLConnection
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        // Set request method
        connection.setRequestMethod("GET");

        // Get response code
        int responseCode = connection.getResponseCode();

        // Check if response code is 200 (HTTP OK)
        if (responseCode == HttpURLConnection.HTTP_OK) {
            // Read response body
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuilder responseContent = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                responseContent.append(inputLine);
            }
            in.close();

            // Parse JSON response or process response content as needed
            String responseBody = responseContent.toString();

            // Set bid information as request attribute
            request.setAttribute("bidInfo", responseBody);

            // Forward request to appropriate JSP page for rendering
            request.getRequestDispatcher("/bidInfo.jsp").forward(request, response);
        } else {
            // Handle non-OK response (e.g., display error message)
            response.getWriter().println("Failed to retrieve bid information. Response code: " + responseCode);
        }
    }
}
