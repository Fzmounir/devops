package hi;

public class Bid {

	    private String bidderName;
	    private double bidAmount;

	    public Bid(String bidderName, double bidAmount) {
	        this.bidderName = bidderName;
	        this.bidAmount = bidAmount;
	    }

	    public String getBidderName() {
	        return bidderName;
	    }

	    public void setBidderName(String bidderName) {
	        this.bidderName = bidderName;
	    }

	    public double getBidAmount() {
	        return bidAmount;
	    }

	    public void setBidAmount(double bidAmount) {
	        this.bidAmount = bidAmount;
	    }
	}

